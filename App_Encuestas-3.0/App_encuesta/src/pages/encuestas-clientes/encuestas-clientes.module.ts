import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EncuestasClientesPage } from './encuestas-clientes';

@NgModule({
  declarations: [
    EncuestasClientesPage,
  ],
  imports: [
    IonicPageModule.forChild(EncuestasClientesPage),
  ],
  exports: [
    EncuestasClientesPage
  ]
})
export class EncuestasClientesPageModule {


}
