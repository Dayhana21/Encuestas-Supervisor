import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {EncuestasClientesPage} from '../encuestas-clientes/encuestas-clientes';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
  export class HomePage {
   usuario:string;
   clave:string;

  constructor(public navCtrl: NavController , private alertCtrl:AlertController) {}

  ionViewDidLoad() {
    this.usuario = "";
    this.clave = "";
}
 //Aqui se colocar el login para la app.
  login(){
  //Validaciones correspondiente del login.
  if(this.usuario == "" &&  this.clave == "" || this.usuario == ""  ||  this.clave == "" ) {
      let alert = this.alertCtrl.create({
        title: 'Alerta',
        subTitle: 'No dejar campos vacios!',
        buttons: ['Ok']
      });
      alert.present();

   }else{

     //this.navCtrl.push(EncuestasClientesPage);
     console.log('Loads');
   }


  }

}
